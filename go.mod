module go.jolheiser.com/drone-vsbuild

go 1.14

require (
	github.com/urfave/cli/v2 v2.2.0
	go.jolheiser.com/beaver v1.0.2
)
