FROM golang:1.15-alpine
LABEL maintainer="john.olheiser@gmail.com"

RUN apk --no-cache add build-base

COPY . /app

WORKDIR /app

RUN make build

ENTRYPOINT ["/app/drone-vsbuild"]