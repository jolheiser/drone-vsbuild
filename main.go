package main

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

func main() {
	app := cli.NewApp()
	app.Name = "Drone VS-Build"
	app.Usage = "Archive VintageStory Mod"
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:    "source",
			Aliases: []string{"s"},
			Usage:   "Path to source files",
			EnvVars: []string{"PLUGIN_SOURCE"},
			Value:   "src/",
		},
		&cli.StringFlag{
			Name:    "mod-info",
			Aliases: []string{"mi", "m"},
			Usage:   "Path to modinfo.json",
			EnvVars: []string{"PLUGIN_MOD_INFO"},
			Value:   "modinfo.json",
		},
		&cli.StringFlag{
			Name:    "destination",
			Aliases: []string{"dest", "d"},
			Usage:   "Output directory",
			EnvVars: []string{"PLUGIN_DESTINATION"},
			Value:   "dist/",
		},
		&cli.BoolFlag{
			Name:    "debug",
			Usage:   "Debug mode",
			EnvVars: []string{"PLUGIN_DEBUG"},
		},
	}
	app.Action = doBuild

	if err := app.Run(os.Args); err != nil {
		beaver.Fatal(err)
	}
}

func doBuild(ctx *cli.Context) error {
	beaver.Console.Format = beaver.FormatOptions{
		LevelPrefix: true,
	}

	if ctx.Bool("debug") {
		beaver.Console.Level = beaver.DEBUG
	}

	destDir := ctx.String("destination")
	beaver.Debugf("Creating output directory at %s", destDir)
	if err := os.MkdirAll(destDir, os.ModePerm); err != nil {
		return err
	}

	modInfoPath := ctx.String("mod-info")
	beaver.Debugf("Reading mod info from %s", modInfoPath)
	modInfo, err := os.Open(modInfoPath)
	if err != nil {
		return err
	}
	defer modInfo.Close()

	var buf bytes.Buffer
	if _, err := io.Copy(&buf, modInfo); err != nil {
		return err
	}
	if _, err := modInfo.Seek(0, 0); err != nil {
		return err
	}

	var mi ModInfo
	if err := json.Unmarshal(buf.Bytes(), &mi); err != nil {
		return err
	}

	archiveName := fmt.Sprintf("%s_v%s.zip", mi.ModID, mi.Version)
	archivePath := path.Join(destDir, archiveName)
	beaver.Debugf("Creating archive at %s", archivePath)

	fi, err := os.Create(archivePath)
	if err != nil {
		return err
	}
	defer fi.Close()

	contents := map[string]*os.File{
		"modinfo.json": modInfo,
	}
	sourcePath := strings.TrimSuffix(ctx.String("source"), "/") + "/"
	if err := filepath.Walk(sourcePath, func(walkPath string, walkInfo os.FileInfo, walkErr error) error {
		if walkErr != nil {
			return walkErr
		}

		if walkInfo.IsDir() {
			return nil
		}

		rel := strings.TrimPrefix(walkPath, sourcePath)
		beaver.Debugf("Found source file at %s -> %s", walkPath, rel)
		fi, err := os.Open(walkPath)
		if err != nil {
			return err
		}

		contents[fmt.Sprintf("src/%s", rel)] = fi
		return nil
	}); err != nil {
		return err
	}

	archive := zip.NewWriter(fi)
	defer archive.Close()
	for rel, fi := range contents {
		beaver.Debugf("Writing file to archive -> %s", rel)

		info, err := fi.Stat()
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return err
		}

		header.Name = rel
		header.Method = zip.Deflate

		wr, err := archive.CreateHeader(header)
		if err != nil {
			return err
		}

		if _, err := io.Copy(wr, fi); err != nil {
			return err
		}

		if err := fi.Close(); err != nil {
			return err
		}
	}

	return nil
}

type ModInfo struct {
	Type             string            `json:"type"`
	ModID            string            `json:"modid"`
	Name             string            `json:"name"`
	Author           string            `json:"author"`
	Description      string            `json:"description"`
	Version          string            `json:"version"`
	Side             Side              `json:"side"`
	RequiredOnClient bool              `json:"requiredOnClient"`
	Dependency       map[string]string `json:"dependency"`
}

type Side string

var (
	Server    Side = "Server"
	Client    Side = "Client"
	Universal Side = "Universal"
)
